#!/usr/bin/env python3
import random
import time
import sys

from anki_export import ApkgReader

USAGE = """
ask.py <file>
"""

if len(sys.argv) != 2:
    print(USAGE)
    exit(1)

path = sys.argv[1]

records = []

with ApkgReader(path) as apkg:
    ids = [row[0] for row 
           in apkg.conn.execute("SELECT id FROM cards").fetchall()]
    chosen_id = random.choice(ids)
    card_raw = apkg.find_card_by_id(chosen_id)
    record = card_raw['data']['note']['data']['record']
    flashcard = (record['Front'], record['Back'])

print()
print('\t', flashcard[0])
input()
print()
print('\t', flashcard[1])
print()
time.sleep(0.6)
